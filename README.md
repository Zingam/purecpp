Experimental Apps with C/C++
==============

```
    print("Experimental projects developed in C/C++")
```

* Endianness - Testing endianness of the system
* Inheritance - C++ inheritance
* MemberFunctionAssignment - Assigning a value to a member function (a setter function example)
* ObjectSlicing - An example of C++ Object Slicing
* OperatorOverloading - An example of overloading iostream operator << for enums (uses <type_traits>)
* ReturnValueOptimization - Demonstration of Return Value Optimization in modern compilers (C++ copy elision)
* Utilities - Low level utility functions


